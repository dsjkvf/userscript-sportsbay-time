// ==UserScript==
// @name        sportsbay-time
// @description adjust time for sportsbay events
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-sportsbay-time
// @downloadURL https://bitbucket.org/dsjkvf/userscript-sportsbay-time/raw/master/spb-time.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-sportsbay-time/raw/master/spb-time.user.js
// @match       https://sportsbay.org/*
// @require     https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.16/moment-timezone-with-data-2012-2022.min.js
// @run-at      document-end
// @grant       GM_addStyle
// @version     0.2
// ==/UserScript==

// Notes
//  - the @grant directive is needed to restore the proper sandbox.

// Set options
const zone_input  = "America/New_York";
const zone_output = "Europe/Tallinn";
const time_format = "HH:mm";

// Clean the page
$("#header").hide()
$("#nwmlay").hide()
// $("#content > div:nth-child(6)").first ().hide ();

// Proceed
function convertTimezone(element) {
    var time_string = element.text().trim();
    var time_input  = moment.tz(time_string, time_format, zone_input);
    var time_output = time_input.tz(zone_output).format("HH:mm");
    element.text(time_output);
}

waitForKeyElements("td.time", convertTimezone);
